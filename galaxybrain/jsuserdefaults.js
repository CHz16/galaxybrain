// jsuerdefaults.js
// Object to manage loading and saving user settings from/to localStorage if available.
// Vaguely inspired by NSUserDefaults ;)

// namespace is a string you can use so that different games in the same domain can use the same keys without clobbering each other.
// defaultValues is an object containing key and value pairs of default values, like:
// { sound: true, numberOfBodies: 4 }
// If you don't have any default values, then just don't pass anything.


export class JSUserDefaults {
    constructor(namespace, defaultValues = {}) {
        // Check if we have localStorage
        this.localStorage = undefined;
        try {
            let localStorage = window.localStorage;
            localStorage.setItem("jsud_storagetest", "whattup");
            localStorage.removeItem("jsud_storagetest");
            this.localStorage = localStorage;
        } catch (e) {
            console.warn("localStorage isn't available, so user data won't be saved after the page is closed");
            this.makeDummyStorage();
        }

        this.namespace = namespace;
        this.values = {};
        this.defaultValues = Object.assign({}, defaultValues);
    }

    // If a value with the given key has been saved through defaults, returns that value.
    // If no value with the given key has been saved but a default value was provided in the constructor, returns that value.
    // Otherwise, returns null.
    get(key) {
        if (this.values[key] !== undefined) {
            return this.values[key];
        }

        let storedValue = this.getStoredValue(key);
        if (storedValue !== null) {
            this.values[key] = storedValue;
            return this.values[key];
        }

        if (this.defaultValues[key] !== undefined) {
            this.values[key] = this.defaultValues[key];
            return this.values[key];
        }

        return null;
    }

    // Saves a key & value in localStorage.
    set(key, value) {
        this.values[key] = value;
        this.storeValue(key, value);
    }

    // Removes a key and its associated value from localStorage.
    remove(key) {
        delete this.values[key];
        this.localStorage.removeItem(this.namespace + key);
    }


    // private

    getStoredValue(key) {
        let storageKey = this.namespace + key;
        return JSON.parse(this.localStorage.getItem(storageKey));
    }

    storeValue(key, value) {
        let storageKey = this.namespace + key;
        let encodedValue = JSON.stringify(value);

        try {
            this.localStorage.setItem(storageKey, encodedValue);
        } catch (e) {
            console.error(`There was some kind of error when trying to save ${key} to localStorage, which probably means we've hit the storage quota. No further data will be saved.`, value);
            this.makeDummyStorage();
        }
    }

    makeDummyStorage() {
        this.localStorage = {
            setItem: function() { },
            getItem: function() { return null; },
            removeItem: function() { }
        };
    }
}
