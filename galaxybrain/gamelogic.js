// gamelogic.js
// Data structures and operations for game state.


import { randomElementFromArray } from "./numbers.js";


let allPatterns = [];
for (let a = 0; a < 6; a++) {
    for (let b = 0; b < 6; b++) {
        for (let c = 0; c < 6; c++) {
            for (let d = 0; d < 6; d++) {
                allPatterns.push([a, b, c, d]);
            }
        }
    }
}

function judgeGuess(inSource, inGuess) {
    let source = [...inSource], guess = [...inGuess];
    let correct = 0, misplaced = 0;
    for (let i = 0; i < 4; i++) {
        if (source[i] == guess[i]) {
            correct += 1;
            source[i] = "x";
            guess[i] = "y";
        }
    }
    for (let i = 0; i < 4; i++) {
        for (let j = 0; j < 4; j++) {
            if (i != j && source[j] == guess[i]) {
                misplaced += 1;
                source[j] = "x";
                guess[i] = "y";
            }
        }
    }
    return `${correct}/${misplaced}`;
}


export class Guess {
    constructor(symbols, response, remaining) {
        this.symbols = symbols;
        this.response = response;
        this.remaining = remaining;
    }
}

export class GameState {
    constructor() {
        this.history = [];
        this.currentSymbols = [null, null, null, null];
        this.selectedSlot = 0;
        this.remainingPatterns = [...allPatterns]; // shallow copy, which is fine as long as we don't modify any of the patterns, so don't do that
    }
}

// Returns true on victory, false otherwise
// Not a class method because JSON serialization/deserialization of class objects doesn't work oops
export function takeGuess(state) {
    let responses = {};
    for (let pattern of state.remainingPatterns) {
        let response = judgeGuess(pattern, state.currentSymbols);
        if (responses[response] !== undefined) {
            responses[response].push(pattern);
        } else {
            responses[response] = [pattern];
        }
    }

    let mostResponses = 0, mostResponseCandidates = [];
    for (let [response, patterns] of Object.entries(responses)) {
        if (patterns.length > mostResponses) {
            mostResponses = patterns.length;
            mostResponseCandidates = [response];
        } else if (patterns.length == mostResponses) {
            mostResponseCandidates.push(response);
        }
    }
    if (mostResponseCandidates.length > 1 && mostResponseCandidates.includes("4/0")) {
        mostResponseCandidates = mostResponseCandidates.filter((el) => el != "4/0");
    }
    let response = randomElementFromArray(mostResponseCandidates);

    state.history.push(new Guess(state.currentSymbols, response, mostResponses));
    state.currentSymbols = [null, null, null, null];
    state.selectedSlot = 0;
    state.remainingPatterns = responses[response];

    return (response == "4/0");
}
