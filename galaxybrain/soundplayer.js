// soundplayer.js
// Object for simple playback of sound effects.

export class SoundPlayer {
    constructor(soundBuffers, context) {
        this.soundBuffers = soundBuffers;
        this.soundChannels = new Object();
        this.context = context;

        this.masterGain = this.context.createGain();
        this.masterGain.gain.setValueAtTime(1, 0);
        this.masterGain.connect(this.context.destination);
    }

    addBuffers(newBuffers) {
        Object.assign(this.soundBuffers, newBuffers);
    }

    play(key, args = {}) {
        let delay = args.delay ?? 0;
        let offset = args.offset ?? 0;
        let loop = args.loop ?? false;

        if (key in this.soundChannels) {
            this.stop(key);
        }

        let sourceNode = this.context.createBufferSource(), gainNode = this.context.createGain();
        this.soundChannels[key] = {sourceNode: sourceNode, gainNode: gainNode};

        sourceNode.buffer = this.soundBuffers[key];
        if (loop) {
            sourceNode.loop = true;
        }
        sourceNode.onended = function() { this.stop(key); }.bind(this);
        sourceNode.connect(gainNode);
        gainNode.connect(this.masterGain);
        sourceNode.start(this.context.currentTime + delay, offset);
    }

    stop(key) {
        if (key in this.soundChannels) {
            this.soundChannels[key].sourceNode.onended = undefined;
            this.soundChannels[key].sourceNode.stop();
            this.soundChannels[key].gainNode.disconnect();
            delete this.soundChannels[key];
        }
    }

    stopAllSounds() {
        for (key in this.soundChannels) {
            this.stop(key);
        }
    }

    fadeOut(key, fadeDuration) {
        if (key in this.soundChannels) {
            this.soundChannels[key].gainNode.gain.setTargetAtTime(0, 0, fadeDuration / 4);
        }
    }

    setVolume(volume) {
        this.masterGain.gain.setValueAtTime(volume, 0);
    }
}
