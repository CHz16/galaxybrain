// replayscene.js
// Controller for the game replay scene.


import { defaults, switchToScene } from "./main.js";


export class ReplaySceneController {
    constructor() {
        document.getElementById("replayreturnbutton").addEventListener("click", function(event) {
            switchToScene("history");
        });
    }


    // main.js hooks

    update(timestamp, dt) {

    }

    startTransitionIn() {
        let replay = document.getElementById("replay");
        replay.scrollTop = 0;
        replay.innerHTML = "";
        let game = defaults.get("replaygame");
        for (let i = 0; i < game.history.length; i++) {
            let guess = document.getElementById("guess").content.firstElementChild.cloneNode(true);
            guess.querySelector("legend").textContent = i + 1;

            let symbols = defaults.get("symbols");
            let symbolDivs = guess.querySelectorAll(".guesssymbol");
            for (let j = 0; j < 4; j++) {
                symbolDivs[j].textContent = symbols[game.history[i].symbols[j]];
            }

            guess.querySelector("button").style.display = "none";
            guess.querySelector(".guessresult").style.display = "flex";
            guess.querySelector(".guessresponse").textContent = game.history[i].response;
            guess.querySelector(".guessremaining").textContent = (game.history[i].response == "4/0") ? "Victory!" : `${game.history[i].remaining} options`;

            replay.appendChild(guess);
        }
    }

    startTransitionOut() {

    }


    // private


}
