// loadingscene.js
// Controller for the loading scene.


import { TEST_MODE } from "./globaldefinitions.js";

import { sounds } from "./mediadata.js";
import { SoundPreloader } from "./soundpreloader.js";

import { defaults, audioContext, soundPlayer, scenes, switchToScene } from "./main.js";

import { GameSceneController } from "./gamescene.js";
import { HistorySceneController } from "./historyscene.js";
import { MenuSceneController } from "./menuscene.js";
import { OptionsSceneController } from "./optionsscene.js";
import { ReplaySceneController } from "./replayscene.js";


const LOADING_PROGRESS_BAR_MAX_SPEED = (TEST_MODE ? 1 : 1 / 250); // % of the bar per millisecond, 1/1000 will fill the bar in 1 second
const LOADING_TARGET_EMOJI_SIZE = 40; // pixels


export class LoadingSceneController {
    constructor() {
        this.mediaLoaded = undefined, this.mediaCount = undefined;
        this.progress = undefined;
        this.errored = false;

        function handleInitialClick(event) {
            // so we can actually play audio later
            audioContext.resume();

            // Kick off the media loaders

            this.mediaLoaded = 0;
            this.mediaCount = sounds.length;
            this.progress = 0;

            let soundLoadCallback = function(preloader, soundsHandled, index) {
                if (this.errored) {
                    return true;
                }
                this.mediaLoaded += 1;
                if (soundsHandled == preloader.sounds.length) {
                    soundPlayer.setVolume(defaults.get("sfxvolume"));
                    soundPlayer.addBuffers(preloader.soundBuffers);
                }
            }.bind(this);
            let soundErrorCallback = function(preloader, soundsHandled, index) {
                this.errored = true;
                document.getElementById("errortext").textContent = `sound: ${preloader.sounds[index].key}`;
                document.getElementById("progressbox").style.display = "none";
                document.getElementById("errorbox").style.display = "block";
                return true;
            }.bind(this);
            let soundPreloader = new SoundPreloader(sounds, audioContext, {base: "sounds/", loadCallback: soundLoadCallback, errorCallback: soundErrorCallback});

            // Now that that's done, we'll do other setup

            document.getElementById("loadingscene").removeEventListener("click", handleInitialClick);
            document.getElementById("loadingscene").style.cursor = "default";
            document.getElementById("clicktoloadbox").style.display = "none";
            document.getElementById("progressbox").style.display = "block";

            for (let button of document.querySelectorAll("button.menubutton")) {
                button.addEventListener("click", function(event) { soundPlayer.play("click"); });
            }

            scenes["menu"] = new MenuSceneController();
            scenes["options"] = new OptionsSceneController();
            scenes["game"] = new GameSceneController();
            scenes["history"] = new HistorySceneController();
            scenes["replay"] = new ReplaySceneController();

            // Dynamically calculate the largest possible font size for the symbols
            let testDiv = document.createElement("div");
            testDiv.textContent = "👀";
            testDiv.style.display = "table-cell";
            let fontSize = 0;
            testDiv.style.fontSize = `${fontSize}px`;
            document.body.appendChild(testDiv);
            while (Math.max(testDiv.clientWidth, testDiv.clientHeight) <= LOADING_TARGET_EMOJI_SIZE) {
                fontSize += 1;
                testDiv.style.fontSize = `${fontSize}px`;
            }
            document.body.removeChild(testDiv);
            document.styleSheets[0].insertRule(`:root { --symbol-size: ${fontSize - 1}px !important; }`);
        }
        document.getElementById("loadingscene").addEventListener("click", handleInitialClick.bind(this));
    }


    // main.js hooks

    update(timestamp, dt) {
        if (this.progress !== undefined) {
            // Move the progress bar forward according to the speed limit
            this.progress = Math.min(this.progress + dt * LOADING_PROGRESS_BAR_MAX_SPEED, this.mediaLoaded / this.mediaCount);
            document.getElementById("progress").value = this.progress;

            if (this.progress == 1) {
                switchToScene("menu");
            }
        }
    }

    startTransitionIn() {

    }

    startTransitionOut() {

    }
}
