// historyscene.js
// Controller for the game history scene.


import { GAME_HISTORY_LENGTH } from "./globaldefinitions.js";

import { defaults, soundPlayer, switchToScene } from "./main.js";


export class HistoryEntry {
    constructor(date, history) {
        this.date = date;
        this.history = history;
    }
}


export class HistorySceneController {
    constructor() {
        document.getElementById("historyreturnbutton").addEventListener("click", function(event) {
            switchToScene("menu");
        });
    }


    // main.js hooks

    update(timestamp, dt) {

    }

    startTransitionIn() {
        document.getElementById("historylength").textContent = GAME_HISTORY_LENGTH;
        let rows = document.getElementById("historytable").querySelectorAll(".historyrow");
        for (let row of rows) {
            row.parentNode.removeChild(row);
        }

        if (defaults.get("bestgame") !== null) {
            document.getElementById("besthistory").appendChild(this.makeTableRowFrom(defaults.get("bestgame")));
        }
        let chronologicalHistoryHeader = document.getElementById("chronologicalhistory").querySelector("tr");
        for (let game of defaults.get("gamehistory")) {
            chronologicalHistoryHeader.insertAdjacentElement("afterend", this.makeTableRowFrom(game));
        }

        defaults.remove("replaygame");
    }

    startTransitionOut() {

    }


    // private

    makeTableRowFrom(game) {
        let row = document.getElementById("historyrow").content.firstElementChild.cloneNode(true);
        let cells = row.querySelectorAll("td");
        cells[0].textContent = game.history.length;
        cells[1].textContent = new Date(game.date).toLocaleString();

        row.addEventListener("click", function(event) {
            defaults.set("replaygame", game);
            switchToScene("replay");
            soundPlayer.play("click");
        });

        return row;
    }
}
