// gamescene.js
// Controller for the game scene.


import { GAME_HISTORY_LENGTH } from "./globaldefinitions.js";

import { GameState, takeGuess } from "./gamelogic.js";
import { HistoryEntry } from "./historyscene.js";

import { defaults, soundPlayer, switchToScene } from "./main.js";


export class GameSceneController {
    constructor() {
        document.getElementById("gamereturnbutton").addEventListener("click", function(event) {
            switchToScene("menu");
        });

        this.activeGuess = document.getElementById("guess").content.firstElementChild.cloneNode(true);
        this.activeGuess.querySelector(".guessresponse").textContent = "Victory!";
        this.activeGuess.querySelector(".guessremaining").style.display = "none";
        this.currentGame = new GameState();

        for (let i = 0; i < 6; i++) {
            document.getElementById(`palettebutton${i + 1}`).addEventListener("click", function(event) {
                let activeDiv = this.activeGuess.querySelector(`div.guesssymbol:nth-of-type(${this.currentGame.selectedSlot + 1})`);
                activeDiv.textContent = defaults.get("symbols")[i];
                this.currentGame.currentSymbols[this.currentGame.selectedSlot] = i;
                this.selectSymbol((this.currentGame.selectedSlot + 1) % 4);
                defaults.set("savedgame", this.currentGame);
                this.updateActiveGuessButton();
                soundPlayer.play("click");
            }.bind(this));
        }

        let symbolDivs = this.activeGuess.querySelectorAll(".guesssymbol");
        for (let i = 0; i < 4; i++) {
            symbolDivs[i].addEventListener("click", function(event) {
                if (!event.target.classList.contains("disabled")) {
                    this.selectSymbol(i);
                    defaults.set("savedgame", this.currentGame);
                }
            }.bind(this));
        }

        this.activeGuess.querySelector("button").addEventListener("click", function(event) {
            soundPlayer.play("click");
            if (takeGuess(this.currentGame)) {
                // victory
                defaults.remove("savedgame");
                let historyEntry = new HistoryEntry(new Date(), this.currentGame.history);
                let gameHistory = defaults.get("gamehistory");
                gameHistory.push(historyEntry);
                if (gameHistory.length > GAME_HISTORY_LENGTH) {
                    gameHistory.shift();
                }
                defaults.set("gamehistory", gameHistory);

                let bestGame = defaults.get("bestgame");
                if (bestGame === null || bestGame.history.length > this.currentGame.history.length) {
                    defaults.set("bestgame", historyEntry);
                }

                this.activeGuess.querySelector("button").style.display = "none";
                this.activeGuess.querySelector(".guessresult").style.display = "flex";
                for (let i = 0; i < 6; i++) {
                    document.getElementById(`palettebutton${i + 1}`).disabled = true;
                }
                for (let symbolDiv of symbolDivs) {
                    symbolDiv.classList.add("disabled");
                }
                soundPlayer.play("victory");
            } else {
                // still guessing
                defaults.set("savedgame", this.currentGame);

                this.selectSymbol(this.currentGame.selectedSlot);
                this.currentGame.currentSymbols = [null, null, null, null];
                for (let symbolDiv of symbolDivs) {
                    symbolDiv.textContent = "";
                }
                this.updateActiveGuessNumber();
                this.updateActiveGuessButton();
                this.insertNewGuess(this.currentGame.history[this.currentGame.history.length - 1]);
            }
        }.bind(this));
    }


    // main.js hooks

    update(timestamp, dt) {

    }

    startTransitionIn() {
        let symbols = defaults.get("symbols");
        document.getElementById("palettebutton1").textContent = symbols[0];
        document.getElementById("palettebutton2").textContent = symbols[1];
        document.getElementById("palettebutton3").textContent = symbols[2];
        document.getElementById("palettebutton4").textContent = symbols[3];
        document.getElementById("palettebutton5").textContent = symbols[4];
        document.getElementById("palettebutton6").textContent = symbols[5];

        // cleanup after potential victory
        this.activeGuess.querySelector("button").style.display = "block";
        this.activeGuess.querySelector(".guessresult").style.display = "none";
        for (let i = 0; i < 6; i++) {
            document.getElementById(`palettebutton${i + 1}`).disabled = false;
        }
        for (let symbolDiv of this.activeGuess.querySelectorAll(".guesssymbol")) {
            symbolDiv.classList.remove("disabled");
        }

        this.currentGame = defaults.get("savedgame") ?? new GameState();

        document.getElementById("guesses").innerHTML = "";
        document.getElementById("guesses").appendChild(this.activeGuess);
        this.updateActiveGuessNumber();
        let symbolDivs = this.activeGuess.querySelectorAll(".guesssymbol");
        for (let i = 0; i < 4; i++) {
            symbolDivs[i].textContent = symbols[this.currentGame.currentSymbols[i]] ?? "";
            if (i == this.currentGame.selectedSlot) {
                symbolDivs[i].classList.add("selected");
            } else {
                symbolDivs[i].classList.remove("selected");
            }
        }
        this.updateActiveGuessButton();

        for (let guess of this.currentGame.history) {
            this.insertNewGuess(guess);
        }
    }

    startTransitionOut() {

    }


    // private

    selectSymbol(n) {
        let symbolDivs = this.activeGuess.querySelectorAll(".guesssymbol");
        for (let i = 0; i < 4; i++) {
            if (i == n) {
                symbolDivs[i].classList.add("selected");
            } else {
                symbolDivs[i].classList.remove("selected");
            }
        }
        this.currentGame.selectedSlot = n;
    }

    updateActiveGuessNumber() {
        this.activeGuess.querySelector("legend").textContent = (this.currentGame.history.length + 1);
    }

    updateActiveGuessButton() {
        let fullGuess = this.currentGame.currentSymbols.every((symbol) => symbol !== null);
        this.activeGuess.querySelector("button").disabled = !fullGuess;
    }

    insertNewGuess(guess) {
        let newGuess = document.getElementById("guess").content.firstElementChild.cloneNode(true);
        newGuess.querySelector("legend").textContent = this.activeGuess.parentNode.childElementCount;

        let symbols = defaults.get("symbols");
        let symbolDivs = newGuess.querySelectorAll(".guesssymbol");
        for (let i = 0; i < 4; i++) {
            symbolDivs[i].textContent = symbols[guess.symbols[i]];
        }

        newGuess.querySelector("button").style.display = "none";
        newGuess.querySelector(".guessresult").style.display = "flex";
        newGuess.querySelector(".guessresponse").textContent = guess.response;
        newGuess.querySelector(".guessremaining").textContent = `${guess.remaining} options`;

        this.activeGuess.insertAdjacentElement("afterend", newGuess);
    }
}
