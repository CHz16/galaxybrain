// optionsscene.js
// Controller for the options scene.


import { generateRandomSymbols } from "./symbols.js";

import { defaults, soundPlayer, switchToScene } from "./main.js";


export class OptionsSceneController {
    constructor() {
        for (let i = 1; i <= 6; i++) {
            document.getElementById(`symbol${i}field`).addEventListener("input", function(event) {
                defaults.set("symbols", [
                    document.getElementById("symbol1field").value,
                    document.getElementById("symbol2field").value,
                    document.getElementById("symbol3field").value,
                    document.getElementById("symbol4field").value,
                    document.getElementById("symbol5field").value,
                    document.getElementById("symbol6field").value
                ]);
            });
        }
        document.getElementById("rerollsymbolsbutton").addEventListener("click", function(event) {
            defaults.set("symbols", generateRandomSymbols());
            this.reloadSymbols();
        }.bind(this));
        document.getElementById("sfxvolumeslider").addEventListener("input", function(event) {
            defaults.set("sfxvolume", event.target.value);
            soundPlayer.setVolume(event.target.value);
        });
        document.getElementById("deletesaveddatabutton").addEventListener("click", function(event) {
            if (window.confirm("Are you sure you want to delete all saved data?")) {
                defaults.remove("bestgame");
                defaults.remove("gamehistory");
                defaults.remove("sfxvolume");
                defaults.remove("replaygame");
                defaults.remove("symbols");
                defaults.remove("savedgame");
                this.startTransitionIn();
            }
        }.bind(this));
        document.getElementById("optionsreturnbutton").addEventListener("click", function(event) {
            switchToScene("menu");
        });
    }


    // main.js hooks

    update(timestamp, dt) {

    }

    startTransitionIn() {
        document.getElementById("sfxvolumeslider").value = defaults.get("sfxvolume");
        this.reloadSymbols();
    }

    startTransitionOut() {

    }


    // private

    reloadSymbols() {
        let symbols = defaults.get("symbols");
        for (let i = 0; i < 6; i++) {
            document.getElementById(`symbol${i + 1}field`).value = symbols[i];
        }
    }
}
