// symbols.js
// Symbol definitions

import { randomElementFromArray } from "./numbers.js";

// Just dumped every emoji from Apple's emoji categories on macOS 10.14.6
// Made a half-hearted attempt at randomly picking skin tones & genders for the emoji that have variants and so this is the longest source file in the repo now oops
// A couple of symbols are commented out because they show up as plain text in FF for me, not emoji
// Slot 1: Smileys & People
// Slot 2: Animals & Nature
// Slot 3: Food & Drink
// Slot 4: Activity
// Slot 5: Travel & Places
// Slot 6: Objects

let slot1 = ["😀", "😃", "😄", "😁", "😆", "😅", "😂", "🤣", "☺️", "😊", "😇", "🙂", "🙃", "😉", "😌", "😍", "🥰", "😘", "😗", "😙", "😚", "😋", "😛", "😝", "😜", "🤪", "🤨", "🧐", "🤓", "😎", "🤩", "🥳", "😏", "😒", "😞", "😔", "😟", "😕", "🙁", "☹️", "😣", "😖", "😫", "😩", "🥺", "😢", "😭", "😤", "😠", "😡", "🤬", "🤯", "😳", "🥵", "🥶", "😱", "😨", "😰", "😥", "😓", "🤗", "🤔", "🤭", "🤫", "🤥", "😶", "😐", "😑", "😬", "🙄", "😯", "😦", "😧", "😮", "😲", "😴", "🤤", "😪", "😵", "🤐", "🥴", "🤢", "🤮", "🤧", "😷", "🤒", "🤕", "🤑", "🤠", "😈", "👿", "👹", "👺", "🤡", "💩", "👻", "💀", "☠️", "👽", "👾", "🤖", "🎃", "😺", "😸", "😹", "😻", "😼", "😽", "🙀", "😿", "😾", {baseSymbol: "🤲", skinTone: true}, {baseSymbol: "👐", skinTone: true}, {baseSymbol: "🙌", skinTone: true}, {baseSymbol: "👏", skinTone: true}, "🤝", {baseSymbol: "👍", skinTone: true}, {baseSymbol: "👎", skinTone: true}, {baseSymbol: "👊", skinTone: true}, {baseSymbol: "✊", skinTone: true}, {baseSymbol: "🤛", skinTone: true}, {baseSymbol: "🤜", skinTone: true}, {baseSymbol: "🤞", skinTone: true}, {baseSymbol: "✌️", skinTone: true}, {baseSymbol: "🤟", skinTone: true}, {baseSymbol: "🤘", skinTone: true}, {baseSymbol: "👌", skinTone: true}, {baseSymbol: "👈", skinTone: true}, {baseSymbol: "👉", skinTone: true}, {baseSymbol: "👆", skinTone: true}, {baseSymbol: "👇", skinTone: true}, {baseSymbol: "☝️", skinTone: true}, {baseSymbol: "✋", skinTone: true}, {baseSymbol: "🤚", skinTone: true}, {baseSymbol: "🖐", skinTone: true}, {baseSymbol: "🖖", skinTone: true}, {baseSymbol: "👋", skinTone: true}, {baseSymbol: "🤙", skinTone: true}, {baseSymbol: "💪", skinTone: true}, {baseSymbol: "🖕", skinTone: true}, {baseSymbol: "✍️", skinTone: true}, {baseSymbol: "🙏", skinTone: true}, {baseSymbol: "🦶", skinTone: true}, {baseSymbol: "🦵", skinTone: true}, "💄", "💋", "👄", "🦷", "👅", {baseSymbol: "👂", skinTone: true}, {baseSymbol: "👃", skinTone: true}, "👣", "👁", "👀", "🧠", "🗣", "👤", "👥", {baseSymbol: "👶", skinTone: true}, {baseSymbols: ["👧", "🧒", "👦"], skinTone: true}, {baseSymbols: ["👩", "🧑", "👨"], skinTone: true}, {baseSymbols: ["👵", "🧓", "👴"], skinTone: true}, {baseSymbol: "👲", skinTone: true}, {baseSymbol: 0x1F473, skinTone: true, gender: true}, {baseSymbol: "🧕", skinTone: true}, {baseSymbol: 0x1F46E, skinTone: true, gender: true}, {baseSymbol: 0x1F477, skinTone: true, gender: true}, {baseSymbol: 0x1F482, skinTone: true, gender: true}, {baseSymbol: 0x1F575, skinTone: true, gender: true}, {job: 0x2695, skinTone: true}, {job: 0x1F33E, skinTone: true}, {job: 0x1F373, skinTone: true}, {job: 0x1F393, skinTone: true}, {job: 0x1F3A4, skinTone: true}, {job: 0x1F3EB, skinTone: true}, {job: 0x1F3ED, skinTone: true}, {job: 0x1F4BB, skinTone: true}, {job: 0x1F4BC, skinTone: true}, {job: 0x1F527, skinTone: true}, {job: 0x1F52C, skinTone: true}, {job: 0x1F3A8, skinTone: true}, {job: 0x1F692, skinTone: true}, {job: 0x2708, skinTone: true}, {job: 0x1F680, skinTone: true}, {job: 0x2696, skinTone: true}, {baseSymbols: ["👰", "🤵"], skinTone: true}, {baseSymbols: ["👸", "🤴"], skinTone: true}, {baseSymbol: 0x1F9B8, skinTone: true, gender: true}, {baseSymbol: 0x1F9B9, skinTone: true, gender: true}, {baseSymbols: ["🤶", "🎅"], skinTone: true}, {baseSymbol: 0x1F9D9, skinTone: true, gender: true}, {baseSymbol: 0x1F9DD, skinTone: true, gender: true}, {baseSymbol: 0x1F9DB, skinTone: true, gender: true}, {baseSymbol: 0x1F9DF, gender: true}, {baseSymbol: 0x1F9DE, gender: true}, {baseSymbol: 0x1F9DC, skinTone: true, gender: true}, {baseSymbol: 0x1F9DA, skinTone: true, gender: true}, {baseSymbol: "👼", skinTone: true}, {baseSymbol: "🤰", skinTone: true}, {baseSymbol: "🤱", skinTone: true}, {baseSymbol: 0x1F647, skinTone: true, gender: true}, {baseSymbol: 0x1F481, skinTone: true, gender: true}, {baseSymbol: 0x1F645, skinTone: true, gender: true}, {baseSymbol: 0x1F646, skinTone: true, gender: true}, {baseSymbol: 0x1F64B, skinTone: true, gender: true}, {baseSymbol: 0x1F926, skinTone: true, gender: true}, {baseSymbol: 0x1F937, skinTone: true, gender: true}, {baseSymbol: 0x1F64E, skinTone: true, gender: true}, {baseSymbol: 0x1F964D, skinTone: true, gender: true}, {baseSymbol: 0x1F487, skinTone: true, gender: true}, {baseSymbol: 0x1F486, skinTone: true, gender: true}, {baseSymbol: 0x1F9D6, skinTone: true, gender: true}, {baseSymbol: "💅", skinTone: true}, {baseSymbol: "🤳", skinTone: true}, {baseSymbols: ["💃", "🕺"], skinTone: true}, {baseSymbol: 0x1F46F, gender: true}, {baseSymbol: "🕴", skinTone: true}, {baseSymbol: 0x1F6B6, skinTone: true, gender: true}, {baseSymbol: 0x1F3C3, skinTone: true, gender: true}, {baseSymbols: ["👫", "👭", "👬"]}, {baseSymbols: ["💑", "👩‍❤️‍👩", "👨‍❤️‍👨"]}, {baseSymbols: ["💏", "👩‍❤️‍💋‍👩", "👨‍❤️‍💋‍👨"]}, {baseSymbols: ["👪", "👨‍👩‍👧", "👨‍👩‍👧‍👦", "👨‍👩‍👦‍👦", "👨‍👩‍👧‍👧", "👩‍👩‍👦", "👩‍👩‍👧", "👩‍👩‍👧‍👦", "👩‍👩‍👦‍👦", "👩‍👩‍👧‍👧", "👨‍👨‍👦", "👨‍👨‍👧", "👨‍👨‍👧‍👦", "👨‍👨‍👦‍👦", "👨‍👨‍👧‍👧", "👩‍👦", "👩‍👧", "👩‍👧‍👦", "👩‍👦‍👦", "👩‍👧‍👧", "👨‍👦", "👨‍👧", "👨‍👧‍👦", "👨‍👦‍👦", "👨‍👧‍👧"]}, "🧶", "🧵", "🧥", "🥼", "👚", "👕", "👖", "👔", "👗", "👙", "👘", "🥿", "👠", "👡", "👢", "👞", "👟", "🥾", "🧦", "🧤", "🧣", "🎩", "🧢", "👒", "🎓", "⛑", "👑", "💍", "👝", "👛", "👜", "💼", "🎒", "🧳", "👓", "🕶", "🥽", "🌂"];
let slot2 = ["🐶", "🐱", "🐭", "🐹", "🐰", "🦊", "🐻", "🐼", "🐨", "🐯", "🦁", "🐮", "🐷", "🐽", "🐸", "🐵", "🙈", "🙉", "🙊", "🐒", "🐔", "🐧", "🐦", "🐤", "🐣", "🐥", "🦆", "🦅", "🦉", "🦇", "🐺", "🐗", "🐴", "🦄", "🐝", "🐛", "🦋", "🐌", "🐞", "🐜", "🦟", "🦗", "🕷", "🕸", "🦂", "🐢", "🐍", "🦎", "🦖", "🦕", "🐙", "🦑", "🦐", "🦞", "🦀", "🐡", "🐠", "🐟", "🐬", "🐳", "🐋", "🦈", "🐊", "🐅", "🐆", "🦓", "🦍", "🐘", "🦛", "🦏", "🐪", "🐫", "🦒", "🦘", "🐃", "🐂", "🐄", "🐎", "🐖", "🐏", "🐑", "🦙", "🐐", "🦌", "🐕", "🐩", "🐈", "🐓", "🦃", "🦚", "🦜", "🦢", "🕊", "🐇", "🦝", "🦡", "🐁", "🐀", "🐿", "🦔", "🐾", "🐉", "🐲", "🌵", "🎄", "🌲", "🌳", "🌴", "🌱", "🌿", "☘️", "🍀", "🎍", "🎋", "🍃", "🍂", "🍁", "🍄", "🐚", "🌾", "💐", "🌷", "🌹", "🥀", "🌺", "🌸", "🌼", "🌻", "🌞", "🌝", "🌛", "🌜", "🌚", "🌕", "🌖", "🌗", "🌘", "🌑", "🌒", "🌓", "🌔", "🌙", "🌎", "🌍", "🌏", "💫", "⭐️", "🌟", "✨", "⚡️", "☄️", "💥", "🔥", "🌪", "🌈", "☀️", "🌤", "⛅️", "🌥", "☁️", "🌦", "🌧", "⛈", "🌩", "🌨", "❄️", "☃️", "⛄️", "🌬", "💨", "💧", "💦", "☔️", "☂️", "🌊", "🌫"];
let slot3 = ["🍏", "🍎", "🍐", "🍊", "🍋", "🍌", "🍉", "🍇", "🍓", "🍈", "🍒", "🍑", "🥭", "🍍", "🥥", "🥝", "🍅", "🍆", "🥑", "🥦", "🥬", "🥒", "🌶", "🌽", "🥕", "🥔", "🍠", "🥐", "🥯", "🍞", "🥖", "🥨", "🧀", "🥚", "🍳", "🥞", "🥓", "🥩", "🍗", "🍖", "🦴", "🌭", "🍔", "🍟", "🍕", "🥪", "🥙", "🌮", "🌯", "🥗", "🥘", "🥫", "🍝", "🍜", "🍲", "🍛", "🍣", "🍱", "🥟", "🍤", "🍙", "🍚", "🍘", "🍥", "🥠", "🥮", "🍢", "🍡", "🍧", "🍨", "🍦", "🥧", "🧁", "🍰", "🎂", "🍮", "🍭", "🍬", "🍫", "🍿", "🍩", "🍪", "🌰", "🥜", "🍯", "🥛", "🍼", "☕️", "🍵", "🥤", "🍶", "🍺", "🍻", "🥂", "🍷", "🥃", "🍸", "🍹", "🍾", "🥄", "🍴", "🍽", "🥣", "🥡", "🥢", "🧂"];
let slot4 = ["⚽️", "🏀", "🏈", "⚾️", "🥎", "🎾", "🏐", "🏉", "🥏", "🎱", "🏓", "🏸", "🏒", "🏑", "🥍", "🏏", "🥅", "⛳️", "🏹", "🎣", "🥊", "🥋", "🎽", "🛹", "🛷", "⛸", "🥌", "🎿", "⛷", "🏂", {baseSymbol: 0x1F3CB, skinTone: true, gender: true}, {baseSymbol: 0x1F93C, skinTone: true, gender: true}, {baseSymbol: 0x1F938, skinTone: true, gender: true}, {baseSymbol: 0x26F9, skinTone: true, gender: true}, "🤺", {baseSymbol: 0x1F93E, skinTone: true, gender: true}, {baseSymbol: 0x1F3CC, skinTone: true, gender: true}, {baseSymbol: 0x1F3C7, skinTone: true}, {baseSymbol: 0x1F9D8, skinTone: true, gender: true}, {baseSymbol: 0x1F3C4, skinTone: true, gender: true}, {baseSymbol: 0x1F3CA, skinTone: true, gender: true}, {baseSymbol: 0x1F93D, skinTone: true, gender: true}, {baseSymbol: 0x1F6A3, skinTone: true, gender: true}, {baseSymbol: 0x1F9D7, skinTone: true, gender: true}, {baseSymbol: 0x1F6B5, skinTone: true, gender: true}, {baseSymbol: 0x1F6B4, skinTone: true, gender: true}, "🏆", "🥇", "🥈", "🥉", "🏅", "🎖", "🏵", "🎗", "🎫", "🎟", "🎪", "🤹‍♀️", "🎭", "🎨", "🎬", "🎤", "🎧", "🎼", "🎹", "🥁", "🎷", "🎺", "🎸", "🎻", "🎲", /* "♟", */ "🎯", "🎳", "🎮", "🎰", "🧩"];
let slot5 = ["🚗", "🚕", "🚙", "🚌", "🚎", "🏎", "🚓", "🚑", "🚒", "🚐", "🚚", "🚛", "🚜", "🛴", "🚲", "🛵", "🏍", "🚨", "🚔", "🚍", "🚘", "🚖", "🚡", "🚠", "🚟", "🚃", "🚋", "🚞", "🚝", "🚄", "🚅", "🚈", "🚂", "🚆", "🚇", "🚊", "🚉", "✈️", "🛫", "🛬", "🛩", "💺", "🛰", "🚀", "🛸", "🚁", "🛶", "⛵️", "🚤", "🛥", "🛳", "⛴", "🚢", "⚓️", "⛽️", "🚧", "🚦", "🚥", "🚏", "🗺", "🗿", "🗽", "🗼", "🏰", "🏯", "🏟", "🎡", "🎢", "🎠", "⛲️", "⛱", "🏖", "🏝", "🏜", "🌋", "⛰", "🏔", "🗻", "🏕", "⛺️", "🏠", "🏡", "🏘", "🏚", "🏗", "🏭", "🏢", "🏬", "🏣", "🏤", "🏥", "🏦", "🏨", "🏪", "🏫", "🏩", "💒", "🏛", "⛪️", "🕌", "🕍", "🕋", "⛩", "🛤", "🛣", "🗾", "🎑", "🏞", "🌅", "🌄", "🌠", "🎇", "🎆", "🌇", "🌆", "🏙", "🌃", "🌌", "🌉", "🌁"];
let slot6 = ["⌚️", "📱", "📲", "💻", "⌨️", "🖥", "🖨", "🖱", "🖲", "🕹", "🗜", "💽", "💾", "💿", "📀", "📼", "📷", "📸", "📹", "🎥", "📽", "🎞", "📞", "☎️", "📟", "📠", "📺", "📻", "🎙", "🎚", "🎛", "🧭", "⏱", "⏲", "⏰", "🕰", "⌛️", "⏳", "📡", "🔋", "🔌", "💡", "🔦", "🕯", "🧯", "🛢", "💸", "💵", "💴", "💶", "💷", "💰", "💳", "💎", "⚖️", "🧰", "🔧", "🔨", /* "⚒", */ "🛠", "⛏", "🔩", "⚙️", "🧱", "⛓", "🧲", "🔫", "💣", "🧨", "🔪", "🗡", "⚔️", "🛡", "🚬", "⚰️", "⚱️", "🏺", "🔮", "📿", "🧿", "💈", "⚗️", "🔭", "🔬", "🕳", "💊", "💉", "🧬", "🦠", "🧫", "🧪", "🌡", "🧹", "🧺", "🧻", "🚽", "🚰", "🚿", "🛁", "🛀", "🧼", "🧽", "🧴", "🛎", "🔑", "🗝", "🚪", "🛋", "🛏", "🛌", "🧸", "🖼", "🛍", "🛒", "🎁", "🎈", "🎏", "🎀", "🎊", "🎉", "🎎", "🏮", "🎐", "🧧", "✉️", "📩", "📨", "📧", "💌", "📥", "📤", "📦", "🏷", "📪", "📫", "📬", "📭", "📮", "📯", "📜", "📃", "📄", "📑", "🧾", "📊", "📈", "📉", "🗒", "🗓", "📆", "📅", "🗑", "📇", "🗃", "🗳", "🗄", "📋", "📁", "📂", "🗂", "🗞", "📰", "📓", "📔", "📒", "📕", "📗", "📘", "📙", "📚", "📖", "🔖", "🧷", "🔗", "📎", "🖇", "📐", "📏", "🧮", "📌", "📍", "✂️", "🖊", "🖋", "✒️", "🖌", "🖍", "📝", "✏️", "🔍", "🔎", "🔏", "🔐", "🔒", "🔓"];

export function generateRandomSymbols() {
    function makeSymbol(s) {
        if (typeof s == "string") {
            return s;
        } else {
            let symbols;
            if (s.baseSymbols !== undefined) {
                symbols = [...s.baseSymbols];
            } else if (s.baseSymbol !== undefined) {
                symbols = [s.baseSymbol];
            } else if (s.job !== undefined) {
                symbols = ["👨", "👩"];
            }
            if (typeof symbols[0] == "number") {
                symbols[0] = String.fromCodePoint(symbols[0]);
            }
            if (s.skinTone) {
                for (let symbol of [...symbols]) {
                    for (let skinToneModifier of [0x1F3FB, 0x1F3FC, 0x1F3FD, 0x1F3FE, 0x1F3FF]) {
                        symbols.push(symbol + String.fromCodePoint(skinToneModifier));
                    }
                }
            }
            if (s.gender || s.job) {
                let suffixes = s.gender ? [0x2640, 0x2642] : [s.job];
                let newSymbols = [];
                for (let symbol of symbols) {
                    for (let suffix of suffixes) {
                        if (suffix < 0x10000) {
                            newSymbols.push(symbol + String.fromCodePoint(0x200D, suffix, 0xFE0F));
                        } else {
                            newSymbols.push(symbol + String.fromCodePoint(0x200D, suffix));
                        }
                    }
                }
                symbols = newSymbols;
            }
            return randomElementFromArray(symbols);
        }
    }

    return [
        makeSymbol(randomElementFromArray(slot1)),
        makeSymbol(randomElementFromArray(slot2)),
        makeSymbol(randomElementFromArray(slot3)),
        makeSymbol(randomElementFromArray(slot4)),
        makeSymbol(randomElementFromArray(slot5)),
        makeSymbol(randomElementFromArray(slot6))
    ]
}
