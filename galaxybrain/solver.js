// solver.js

// Just a quick brute force DFS solver I wrote in response to this tweet: <https://twitter.com/antimagical/status/1415765419885879297>. I'm just adding it to the repo because I don't feel like trashing this now that I've written it.
// Given a target pattern, this attempts to force the AI to pick that pattern by never guessing something that would eliminate it as an option.
// Since Galaxybrain's default AI is nondeterministic in the case of ties, it should probably be modified to be deterministic if you want to use this. A recommended modification would be to change the line `let response = randomElementFromArray(mostResponseCandidates);` to `let response = mostResponseCandidates.sort()[0];`.


import { GameState, takeGuess } from "./gamelogic.js";

let target = [0, 0, 0, 0];


let allPatterns = [];
for (let a = 0; a < 6; a++) {
    for (let b = 0; b < 6; b++) {
        for (let c = 0; c < 6; c++) {
            for (let d = 0; d < 6; d++) {
                allPatterns.push([a, b, c, d]);
            }
        }
    }
}

function patternsEqual(p1, p2) {
    return (p1[0] == p2[0] && p1[1] == p2[1] && p1[2] == p2[2] && p1[3] == p2[3])
}


let stack = [{state: new GameState(), nextPattern: 0, history: []}];
outer: while (stack.length > 0) {
    let {state, nextPattern, history} = stack.pop();

    // Add the next guess from the current state to the stack
    if (nextPattern < allPatterns.length - 1) {
        stack.push({state: state, nextPattern: nextPattern + 1, history: history});
    }

    // If we've already guessed this current pattern, then skip it
    if (history.includes(nextPattern) ) {
        continue;
    }

    // Take the guess
    let clonedState = JSON.parse(JSON.stringify(state));
    clonedState.currentSymbols = [...allPatterns[nextPattern]];
    takeGuess(clonedState);

    // If that guess results in only one pattern left, check if it's the one we're looking for, and either call it a win or move on
    if (clonedState.remainingPatterns.length == 1) {
        if (patternsEqual(target, clonedState.remainingPatterns[0])) {
            console.log(history.map(x => allPatterns[x]), allPatterns[nextPattern]);
            break;
        } else {
            continue;
        }
    }

    // I dunno if it's actually possible to end up in a contradiction but it doesn't hurt to check
    if (clonedState.remainingPatterns.length == 0) {
        continue;
    }

    // If the guess didn't actually eliminate anything new then just move on
    if (clonedState.remainingPatterns.length == state.remainingPatterns.length) {
        continue;
    }

    // Check if the target pattern still remains, and if so, add this state to the stack to begin searching from
    for (let pattern of clonedState.remainingPatterns) {
        if (patternsEqual(target, pattern)) {
            stack.push({state: clonedState, nextPattern: 0, history: [...history, nextPattern]});
            continue outer;
        }
    }
}
