// main.js
// Initial UI setup and global behavior.


import { TEST_MODE } from "./globaldefinitions.js";
import { generateRandomSymbols } from "./symbols.js";

import { JSUserDefaults } from "./jsuserdefaults.js";
import { SoundPlayer } from "./soundplayer.js";

import { LoadingSceneController } from "./loadingscene.js";


//
// GLOBALS
//

export let defaults = new JSUserDefaults("galaxybrain", {
    sfxvolume: 1.0,
    symbols: generateRandomSymbols(),
    gamehistory: []
});

export let audioContext;
export let soundPlayer;

export let scenes = [];
let currentScene = "loading";

let lastUpdateTime;


//
// INITIALIZATION
//

if (TEST_MODE) {
    let testCSS = document.createElement("link");
    testCSS.rel = "stylesheet";
    testCSS.type = "text/css";
    testCSS.href = "test.css";
    document.querySelector("head").appendChild(testCSS);

    let debugP = document.createElement("p");
    debugP.id = "debug";
    document.body.appendChild(debugP);
}

export function debug(s) {
    document.getElementById("debug").innerHTML += `<br />${s}`;
}


window.addEventListener("load", init);
function init() {
    audioContext = new (window.AudioContext || window.webkitAudioContext)();
    // resumed in the initial click handler on LoadingSceneController
    soundPlayer = new SoundPlayer({}, audioContext);

    scenes["loading"] = new LoadingSceneController();
    scenes["loading"].startTransitionIn();
    window.requestAnimationFrame(update);

    // All resource loading and other UI setup happens in LoadingSceneController
}

function update(timestamp) {
    if (lastUpdateTime === undefined) {
        lastUpdateTime = timestamp;
    }
    let dt = timestamp - lastUpdateTime;
    lastUpdateTime = timestamp;
    scenes[currentScene].update(timestamp, dt);

    window.requestAnimationFrame(update);
}





export function switchToScene(scene) {
    if (currentScene !== undefined) {
        scenes[currentScene].startTransitionOut();
        let outgoingSceneContainer = document.getElementById(`${currentScene}scene`);
        outgoingSceneContainer.style.zIndex = 10;
        outgoingSceneContainer.classList.remove("fadescenein");
        outgoingSceneContainer.classList.add("fadesceneout");
    }
    currentScene = scene;
    scenes[currentScene].startTransitionIn();
    let incomingSceneContainer = document.getElementById(`${currentScene}scene`);
    incomingSceneContainer.style.zIndex = 20;
    incomingSceneContainer.classList.remove("fadesceneout");
    incomingSceneContainer.classList.add("fadescenein");
}
