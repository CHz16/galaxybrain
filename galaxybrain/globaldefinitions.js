// globaldefinitions.js
// Exported configuration constants


export function recursiveFreeze(obj) {
    for (let propertyName of Object.getOwnPropertyNames(obj)) {
        if (typeof obj[propertyName] == "object") {
            recursiveFreeze(obj[propertyName]);
        }
    }
    return Object.freeze(obj);
}


export const CONTAINER_WIDTH = 600, CONTAINER_HEIGHT = 600;
export const BORDER_WIDTH = 3;

export const GAME_HISTORY_LENGTH = 50;


// Calculated constants
export const TEST_MODE = (window.location.protocol == "file:") && ((new URL(window.location)).searchParams.get("live") === null);
