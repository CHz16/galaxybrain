// menuscene.js
// Controller for the menu scene.


import { defaults, soundPlayer, switchToScene } from "./main.js";


export class MenuSceneController {
    constructor() {
        document.getElementById("resumegamebutton").addEventListener("click", function(event) {
            switchToScene("game");
        });
        document.getElementById("newgamebutton").addEventListener("click", function(event) {
            defaults.remove("savedgame");
            switchToScene("game");
        });
        document.getElementById("optionsbutton").addEventListener("click", function(event) {
            switchToScene("options");
        });
        document.getElementById("historybutton").addEventListener("click", function(event) {
            switchToScene("history");
        });
    }


    // main.js hooks

    update(timestamp, dt) {

    }

    startTransitionIn() {
        document.getElementById("resumegamebutton").style.display = (defaults.get("savedgame") !== null) ? "block" : "none";
    }

    startTransitionOut() {

    }
}
