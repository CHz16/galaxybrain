Galaxybrain
===========

Mastermind® except it cheats, made for [GAMES MADE QUICK??? ＧＯ！！五の次！〜☆](https://itch.io/jam/games-made-quick-go-next). Playable here: https://chz.itch.io/galaxybrain

Inspired by jan Misali's video "[hangman is a weird game](https://www.youtube.com/watch?v=le5uGqHKll8)."

Everything here is by me and released under MIT (see LICENSE.txt), except the background texture `45-degree-fabric-light.png`, which is originally ["45-Degree Fabric by Atle Mo for Toptal Subtle Patterns](https://www.toptal.com/designers/subtlepatterns/awesome-pattern/). The image file here is [a transparent modification by Mike Hearn for Transparent Textures](https://www.transparenttextures.com/45-degree-fabric-light.html).
